<?php
####################################################################
# LinkX - Copyright � 2006 JMB Software, Inc. All Rights Reserved. #
# This file may not be redistributed in whole or significant part. #
# LINKX IS NOT FREE SOFTWARE                                       #
# http://www.jmbsoft.com/ # http://www.jmbsoft.com/license.php     #
####################################################################

if( !is_file('scanner.php') )
{
    echo "This file must be located in the admin directory of your LinkX installation";
    exit;
}

define('LINKX', TRUE);

require_once('../includes/common.php');
require_once("{$GLOBALS['BASE_DIR']}/includes/mysql.class.php");
require_once("{$GLOBALS['BASE_DIR']}/admin/includes/functions.php");

SetupRequest();

$DB = new DB($C['db_hostname'], $C['db_username'], $C['db_password'], $C['db_name']);
$DB->Connect();

@set_time_limit(0);

if( $_SERVER['REQUEST_METHOD'] == 'POST' )
{
    ResetInstall();
}
else
{
    DisplayMain();
}

$DB->Disconnect();

function ResetInstall()
{
    global $DB, $C;
    
    IniParse("{$GLOBALS['BASE_DIR']}/includes/tables.php", TRUE, $tables);
    
    foreach( $tables as $table => $create )
    {
        $DB->Update('DROP TABLE IF EXISTS #', array($table));
    }
   
    FileWrite("{$GLOBALS['BASE_DIR']}/includes/config.php", "<?php\n\$C = array();\n?>");
    
    echo "Your LinkX installation has been reset<br />" .
         "Upload the install.php script and access it through your browser to re-initialize the software";
}

function DisplayMain()
{
   
echo <<<OUT
<html>
<head>
  <title>Reset LinkX Installation</title>
  <style>
  body, form, input { font-family: Tahoma; font-size: 9pt; }
  </style>
</head>
<body>
<center>
<b>Press the button below to reset your LinkX installation to it's default state.<br />
This will delete all of the data and settings that you have configured up to this point.</b>
<form method="POST" action="reset-install.php" style="margin-top: 20px;" onsubmit="return confirm('Are you sure you want to reset your LinkX installation?')">
<input type="submit" value="Reset LinkX Installation" style="margin-top: 10px;">
</form>
</center>

</body>
</html>
OUT;
}

?>
