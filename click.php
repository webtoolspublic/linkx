<?php
####################################################################
# LinkX - Copyright � 2006 JMB Software, Inc. All Rights Reserved. #
# This file may not be redistributed in whole or significant part. #
# LINKX IS NOT FREE SOFTWARE                                       #
# http://www.jmbsoft.com/ # http://www.jmbsoft.com/license.php     #
####################################################################

if( !defined('E_STRICT') ) define('E_STRICT', 2048);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);

require_once('includes/config.php');
require_once('includes/mysql.class.php');

if( $_GET['id'] )
{
    if( !isset($_COOKIE['linkx_click']) || !strstr(",{$_COOKIE['linkx_click']},", ",{$_GET['id']},") )
    {
        $DB = new DB($C['db_hostname'], $C['db_username'], $C['db_password'], $C['db_name']);
        $DB->Connect();
        $DB->Update('UPDATE lx_links SET clicks=clicks+1 WHERE link_id=?', array($_GET['id']));
        $DB->Disconnect();

        $cookie = isset($_COOKIE['linkx_click']) ? "{$_COOKIE['linkx_click']},{$_GET['id']}" : $_GET['id'];
        setcookie('linkx_click', $cookie, time() + 604800, '/', $C['cookie_domain']);
    }

    if( !isset($_GET['f']) )
    {
        header("Location: {$_GET['u']}");
    }
}
else
{
    header("Location: {$C['base_url']}/");
}

?>